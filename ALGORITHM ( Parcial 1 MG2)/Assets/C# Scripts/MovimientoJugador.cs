using System.Collections.Specialized;
//using System.Diagnostics;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 0.45f;
    private float initialSize;
    private int i = 0;
    public bool floored = true;
    public Vector3 Gravedad;


    public GameObject levelControl;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        Physics.gravity = Gravedad;
    }

    void Update()
    {

        GetInput();


    }

    private void GetInput()
    {
        Jump();
        Duck();
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {

            if (floored)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);

            }
            else
            {
                rb.AddForce(new Vector3(0, jumpForce * .4F, 0), ForceMode.Impulse);
            }

        }



    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {

        /*if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
            BarraTemprizador.TiempoPasado = BarraTemprizador.TiempoOriginal;
            levelControl.GetComponent<EndRunSequence>().enabled = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }

        if (collision.gameObject.CompareTag("Gasolina"))
        {
            Destroy(collision.gameObject);
            BarraTemprizador.TiempoOriginal *= 1.5f;
        }

        if (collision.gameObject.CompareTag("Moneda"))
        {
            Destroy(collision.gameObject);
            ColeccionControl.ContadorMoneda += 1;
        }*/
    }


    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            floored = false;
        }
    }
}