using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class NewRecord : MonoBehaviour
{
    public static int recordkilometro;
    // Start is called before the first frame update
    void Start()
    {
        LeerRecord();
    }

    // Update is called once per frame
    void Update()
    {
        if (Game_Hud.gameOver)
        {
            if (Game_Hud.DistanciaRedondeada > recordkilometro)
            {
                recordkilometro = Game_Hud.DistanciaRedondeada;
                GrabarRecord();
            }

        }
    }

    void LeerRecord()
    {
        string filePath = Application.dataPath + "/Record.bin";

        using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
        {
            recordkilometro = reader.ReadInt32();
        }

    }

    void GrabarRecord()
    {
        string filePath = Application.dataPath + "/Record.bin";

        using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
        {
            writer.Write(recordkilometro);
        }

    }




}
