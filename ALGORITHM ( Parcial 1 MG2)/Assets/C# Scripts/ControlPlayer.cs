using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using TMPro;


public class ControlPlayer : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 45f;
    private float initialSize;
    private float PosicionY;
    private int i = 0;
    public bool floored = true;
    public Vector3 Gravedad;
    public float TiempoInvisible = 0;
    public TMP_Text txt_CantVidas;
    

    // Start is called before the first frame update
    void Start()
    {
        
        rb = GetComponent<Rigidbody>();
        PosicionY = rb.transform.position.y;
        initialSize = rb.transform.localScale.y;
        Physics.gravity = Gravedad;

    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    
    }
    private void GetInput()
    {
        Jump();
        Duck();
        Invisible();
    }

    private void Jump()
    {
        
        if (Input.GetKeyDown(KeyCode.W))
        {
            
            if (floored)
            {
                
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);

            }
            

        }

    }
    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Invisible() 
    {
        if (TiempoInvisible > 0)
        {
            TiempoInvisible -= Time.deltaTime;
        }
        else
        {
            if (TiempoInvisible < 0)
            {
                rb.transform.position = new Vector3(rb.transform.position.x, PosicionY, rb.transform.position.z);
                TiempoInvisible = 0;
                floored = true;
            }
        }

    }

    public void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Pesado"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Game_Hud.gameOver = true;
            //BarraTemprizador.TiempoPasado = BarraTemprizador.TiempoOriginal;
            //levelControl.GetComponent<EndRunSequence>().enabled = true;
        }
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Game_Hud.Vidas_Jugador -= 1;
            Destroy(collision.gameObject);
            txt_CantVidas.text = " " + Game_Hud.Vidas_Jugador ;
            if(Game_Hud.Vidas_Jugador <= 0)
            {
                Destroy(this.gameObject);
                Game_Hud.gameOver = true;
            }
        }
        if (collision.gameObject.CompareTag("Absorber"))
        {
            Destroy(collision.gameObject);
            rb.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y - 10, rb.transform.position.z);
            TiempoInvisible = 3;
        }
        
        if (collision.gameObject.CompareTag("Bateria"))
        {
            Destroy(collision.gameObject);
            BarraTemproizador.TiempoOriginal *= 1.5f;
        }

        if (collision.gameObject.CompareTag("Piso"))
        {
            floored = true;
        }

        
    }


    private void OnCollisionExit(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Piso"))
        {
            
            floored = false;
        }
    }


   
}
