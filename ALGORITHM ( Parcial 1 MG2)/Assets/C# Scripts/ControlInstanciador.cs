using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class ControlInstanciador : MonoBehaviour
{
    public List<GameObject> Objetos;
    public GameObject instantiatePos;
    public float TiempoDespertarObjetos;
    private float AlturaObjetos;
    private Vector3 PosicionSalida;

    // Start is called before the first frame update
    void Start()
    {
        Enemy1Control.VelocidadEnemigo = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        DespertarObjetos();
    }

    private void DespertarObjetos()
    {
        TiempoDespertarObjetos -= Time.deltaTime;

        if (TiempoDespertarObjetos <= 0)
        {
            AlturaObjetos = UnityEngine.Random.Range(0, 3);
            PosicionSalida = new Vector3(instantiatePos.transform.position.x, AlturaObjetos, instantiatePos.transform.position.z);
            Instantiate(Objetos[UnityEngine.Random.Range(0, Objetos.Count)], PosicionSalida, instantiatePos.transform.rotation);
            TiempoDespertarObjetos = UnityEngine.Random.Range(1, 3);
        }
    }




}
