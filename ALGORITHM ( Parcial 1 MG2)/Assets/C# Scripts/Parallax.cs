using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {
        
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        if (Game_Hud.gameOver == false)
        {
            if (BarraTemproizador.TiempoPasado < BarraTemproizador.TiempoOriginal)
            {

                transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
                if (gameObject.CompareTag("Layer2"))
                {
                    if (transform.localPosition.x < -30.72f)
                    {
                        transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
                    }
                }
                if (gameObject.CompareTag("Layer3"))
                {
                    if (transform.localPosition.x < -24.52f)
                    {
                        transform.localPosition = new Vector3(12.46f, transform.localPosition.y, transform.localPosition.z);
                    }
                }
                if (gameObject.CompareTag("Luces"))
                {
                    if (transform.localPosition.x < -17.5f)
                    {
                        transform.localPosition = new Vector3(-4.37f, transform.localPosition.y, transform.localPosition.z);
                    }
                }
                if (gameObject.CompareTag("Cielo"))
                {
                    if (transform.localPosition.x < -20)
                    {
                        transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
                    }
                }
            }
        }
        

    }
}
