using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Game_Hud : MonoBehaviour
{
    public GameObject GameEndScreen;
    public GameObject DuringGameplay;
    public static bool gameOver = false;
    public static int Vidas_Jugador = 3;
    public TMP_Text distanceText;
    public TMP_Text EnddistanceText;
    private float distance = 0;
    public static int DistanciaRedondeada;
    public TMP_Text EndRecordText;

    // Start is called before the first frame update
    void Start()
    {
        DuringGameplay.gameObject.SetActive(true);
        gameOver = false;
        Vidas_Jugador = 3;
        gameOver = false;
        distance = 0;
        DistanciaRedondeada = Mathf.RoundToInt(distance);
        distanceText.text = DistanciaRedondeada.ToString() + " km";
    }

    // Update is called once per frame
    void Update()
    {
        if(Vidas_Jugador <= 0 && BarraTemproizador.TiempoPasado >= BarraTemproizador.TiempoOriginal)
        {
            gameOver = true;
        }

        if (gameOver)
        {
            DuringGameplay.gameObject.SetActive(false);
            Time.timeScale = 0;
            GameEndScreen.gameObject.SetActive(true);
            EnddistanceText.text = " " + DistanciaRedondeada + " km";
            EndRecordText.text = " " + NewRecord.recordkilometro + " km";
            if (Input.GetKey(KeyCode.E))
            {
                Application.Quit();

            }
        }
        else
        {
            distance += Time.deltaTime;
            DistanciaRedondeada = Mathf.RoundToInt(distance);
            distanceText.text = DistanciaRedondeada.ToString() + " km";
        }
    }
    
}
